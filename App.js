import React, { Component } from "react";
import "./App.css";
import Navbar from "./book-component/navbar";
import { Route, Switch, Redirect, Link } from "react-router-dom";
import BooksComp from "./book-component/books";

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <div className="content">
          <Switch>
            <Route path="/books/:genre/:newarrival?" component={BooksComp} />
            <Route path="/books" component={BooksComp} />{" "}
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
