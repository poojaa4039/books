import React, { Component } from "react";
class CheckBoxes extends Component {
  state = {};
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { bestSellerOptions, languageOptions } = this.props;
    let cb1 = bestSellerOptions.options.find(
      (n1) => n1.refineValue === input.name
    );
    if (cb1) {
      cb1.isSelected = input.checked;
      bestSellerOptions.selected = input.name;
    }

    let cb2 = languageOptions.options.find(
      (n1) => n1.refineValue === input.name
    );
    if (cb2) {
      cb2.isSelected = input.checked;
      languageOptions.selected = input.name;
    }
    this.props.onOptionChange(bestSellerOptions, languageOptions);
  };

  render() {
    const { bestSellerOptions, languageOptions } = this.props;
    return (
      <div className="text-center">
        <div className="padding border">
          <strong>Options</strong>
        </div>
        <div className="padding border">
          <strong>{bestSellerOptions.display}</strong> <br />
          {bestSellerOptions.options.map((c) => (
            <div className="form-check" key={c.refineValue}>
              <input
                value={c.isSelected}
                onChange={this.handleChange}
                type="checkbox"
                name={c.refineValue}
                checked={c.isSelected}
                className="form-check-input"
              />
              <label htmlFor={c.refineValue} className="form-check-label">
                {" "}
                {c.refineValue + "(" + c.totalNum + ")"}
              </label>
            </div>
          ))}
        </div>
        <div className="padding border">
          <strong>{languageOptions.display}</strong> <br />
          {languageOptions.options.map((c, index) => (
            <div className="form-check" key={c.refineValue}>
              <input
                value={c.isSelected}
                onChange={this.handleChange}
                type="checkbox"
                name={c.refineValue}
                checked={c.isSelected}
                className="form-check-input"
              />
              <label htmlFor={c.refineValue} className="form-check-label">
                {" "}
                {c.refineValue + "(" + c.totalNum + ")"}
              </label>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default CheckBoxes;
