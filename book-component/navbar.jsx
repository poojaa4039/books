import React, { Component } from "react";
import { Link } from "react-router-dom";
class Navbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
        <Link className="navbar-brand" to="/books">
          BookSite
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#nav-main"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="nav-main">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className="nav-link" to="/books?newarrival=Yes">
                New Arrivals
              </Link>
            </li>

            <li className="nav-item dropdown">
              <Link
                className="nav-link dropdown-toggle"
                to="#"
                id="navDropdown"
                data-toggle="dropdown"
              >
                Books by Genre{" "}
              </Link>
              <div className="dropdown-menu" aria-labelledby="navDropdown">
                <Link className="dropdown-item" to="/books/Fiction">
                  Fiction
                </Link>
                <Link className="dropdown-item" to="/books/Children">
                  Children
                </Link>
                <Link className="dropdown-item" to="/books/Mystery">
                  Mystery
                </Link>
                <Link className="dropdown-item" to="/books/Management">
                  Management
                </Link>
                <Link className="dropdown-item" to="/books/Self Help">
                  Self Help
                </Link>
              </div>
            </li>
            <li className="nav-item">
              <Link className="nav-link active" to="/books">
                All Books
              </Link>
            </li>
          </ul>
          <span className="navbar-text">LOGIN</span>
        </div>
      </nav>
    );
  }
}

export default Navbar;
