import React, { Component } from "react";
import axios from "../service/httpService";
import config from "../config.json";
import queryString from "query-string";
import CheckBoxes from "./checkbox";
class BooksComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      books: [],
      pageInfo: "",
      refineOptions: {},
    };
  }
  async componentDidMount() {
    let genre = this.props.match.params.genre;
    let url = config.accessedURL;
    url = genre ? url + "/" + genre : url;
    const { data: books } = await axios.get(url + this.props.location.search);
    this.setState({
      books: books.data,
      pageInfo: books.pageInfo,
      refineOptions: books.refineOptions,
    });
  }
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      if (params) params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callURL = (params, newarrival, selectedBestSeller, selectedLanguages) => {
    let path = "/books";
    let genre = this.props.match.params.genre;
    if (genre) path = path + "/" + genre;
    params = this.addToParams(params, "language", selectedLanguages);
    params = this.addToParams(params, "bestseller", selectedBestSeller);
    params = this.addToParams(params, "newarrival", newarrival);
    this.props.history.push({ pathname: path, search: params });
  };
  async componentDidUpdate(prevProps) {
    if (prevProps != this.props) {
      let genre = this.props.match.params.genre;
      let url = config.accessedURL;
      url = genre ? url + "/" + genre : url;
      const { data: books } = await axios.get(url + this.props.location.search);
      this.setState({
        books: books.data,
        pageInfo: books.pageInfo,
        refineOptions: books.refineOptions,
      });
    }
  }
  goToPage = (x) => {
    let { newarrival, page, language, bestseller } = queryString.parse(
      this.props.location.search
    );
    let currPage = page ? +page : 1;
    currPage = currPage + x;
    let params = "";
    params = this.addToParams(params, "page", currPage);
    this.callURL(params, newarrival, bestseller, language);
  };
  createBestSellerOptions = (bestseller, checkedBestsellers) => {
    // put condition as it is rendering before is set in state and giving it as undefined
    if (bestseller) {
      let options = bestseller.map((opt) => {
        if (opt.totalNum > 0)
          return {
            totalNum: opt.totalNum,
            refineValue: opt.refineValue,
            isSelected: false,
          };
      });
      options = options.filter((opt) => opt);

      let options2 = { options: options, display: "BestSeller", selected: "" };
      let bestSellerArray = checkedBestsellers.split(",");
      for (let i = 0; i < bestSellerArray.length; i++) {
        let obj = options2.options.find(
          (opt) => opt.refineValue === bestSellerArray[i]
        );
        if (obj) {
          obj.isSelected = true;
        }
        options2.selected = options2.selected + bestSellerArray[i];
      }
      return options2;
    }
  };
  createLanguageOptions = (languages, checkedLanguages) => {
    // put condition as it is rendering before is set in state and giving it as undefined
    if (languages) {
      let options = languages.map((opt) => {
        if (opt.totalNum > 0)
          return {
            totalNum: opt.totalNum,
            refineValue: opt.refineValue,
            isSelected: false,
          };
      });
      options = options.filter((opt) => opt);

      let options2 = { options: options, display: "Language", selected: "" };
      let languageArray = checkedLanguages.split(",");
      for (let i = 0; i < languageArray.length; i++) {
        let obj = options2.options.find(
          (opt) => opt.refineValue === languageArray[i]
        );
        if (obj) {
          obj.isSelected = true;
        }
        options2.selected = options2.selected + languageArray[i];
      }
      return options2;
    }
  };
  handleOptionChange = (checkedBestSellers, checkedLanguage) => {
    let { newarrival } = this.props.location.search;
    let filterBestSeller = checkedBestSellers.options.filter(
      (n1) => n1.isSelected
    );
    // console.log("check", checkedBestSellers);

    let arrayBestSeller = filterBestSeller.map((n1) => n1.refineValue);
    let selectedBestSeller = arrayBestSeller.join(",");
    let filterlanguages = checkedLanguage.options.filter((n1) => n1.isSelected);
    let arrayLanguage = filterlanguages.map((n1) => n1.refineValue);
    let selectedLanguages = arrayLanguage.join(",");
    this.callURL("", newarrival, selectedBestSeller, selectedLanguages);
  };

  render() {
    let { books, pageInfo, refineOptions } = this.state;
    let genre = this.props.match.params.genre;
    // console.log("vook", books, "refine", refineOptions);
    let { newarrival, page, bestseller, language } = queryString.parse(
      this.props.location.search
    );
    page = page ? +page : 1;
    newarrival = newarrival ? newarrival : "";
    bestseller = bestseller ? bestseller : "";
    language = language ? language : "";

    let bestSellerOptions = this.createBestSellerOptions(
      refineOptions.bestseller,
      bestseller
    );
    let languageOptions = this.createLanguageOptions(
      refineOptions.language,
      language
    );
    //console.log("best", bestSellerOptions);
    // console.log("lanf", languageOptions);

    return (
      <div className="container">
        <div className="row padding">
          <div className="col-2">
            {bestSellerOptions && languageOptions ? (
              <CheckBoxes
                bestSellerOptions={bestSellerOptions}
                languageOptions={languageOptions}
                onOptionChange={this.handleOptionChange}
              />
            ) : (
              ""
            )}
          </div>
          <div className="col-10">
            <div className="container mt-4">
              <strong>
                {(pageInfo.pageNumber - 1) * pageInfo.numOfItems + 1} to{" "}
                {pageInfo.pageNumber * pageInfo.numOfItems} of{" "}
                {pageInfo.totalItemCount}
              </strong>
              <div className="row text-center">
                <div className="col-3 border  bg-primary ">Title</div>
                <div className="col-3 border  bg-primary ">Author</div>
                <div className="col-2 border  bg-primary ">Language</div>
                <div className="col-2 border  bg-primary ">Genre</div>
                <div className="col-1 border  bg-primary ">Price</div>
                <div className="col-1 border  bg-primary ">Best..</div>
              </div>

              {books.map((book) => (
                <div className="row text-center" key={book.name}>
                  <div className="col-3 border">{book.name}</div>
                  <div className="col-3 border">{book.author}</div>
                  <div className="col-2 border">{book.language}</div>
                  <div className="col-2 border">{book.genre}</div>
                  <div className="col-1 border">{book.price}</div>
                  <div className="col-1 border">{book.bestseller}</div>
                </div>
              ))}
            </div>
            {page > 1 && (
              <button
                className="btn btn-primary m-2"
                onClick={() => this.goToPage(-1)}
              >
                Previous
              </button>
            )}
            {page < pageInfo.numberOfPages && (
              <button
                className="btn btn-primary"
                onClick={() => this.goToPage(1)}
              >
                Next
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default BooksComp;
