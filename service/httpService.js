import axios from "axios";

axios.interceptors.response.use(null, (error) => {
  const expectedErr =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;
  if (!expectedErr) {
    console.log("Logging error", error);
    alert("unexpected error");
    return Promise.reject(error);
  }
});
export default {
  get: axios.get,
  post: axios.post,
  delete: axios.delete,
  put: axios.put,
};
